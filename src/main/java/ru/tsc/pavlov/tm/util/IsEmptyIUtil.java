package ru.tsc.pavlov.tm.util;

public interface IsEmptyIUtil {

    static boolean IsEmpty(final String value){
        if (value == null || value.isEmpty()) return true;
        else return false;
    }

    static boolean IsEmpty(final String [] value) {
        if (value == null || value.length == 0) return true;
        else return false;
    }

}
