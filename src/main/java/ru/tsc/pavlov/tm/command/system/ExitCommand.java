package ru.tsc.pavlov.tm.command.system;

import ru.tsc.pavlov.tm.api.service.ServiceLocator;
import ru.tsc.pavlov.tm.command.AbstractCommand;

public class ExitCommand extends AbstractCommand {

    @Override
    public String getName() {
        return "exit";
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return "Close application";
    }

    @Override
    public void execute() {
        System.out.println("Closing Application");
        System.exit(0);
    }

}
