package ru.tsc.pavlov.tm.command.task;

import ru.tsc.pavlov.tm.constant.TerminalConst;
import ru.tsc.pavlov.tm.exception.entity.TaskNotFoundException;
import ru.tsc.pavlov.tm.model.Task;
import ru.tsc.pavlov.tm.util.TerminalUtil;

public class TaskFinishByIndexCommand extends AbstractTaskCommand {

    @Override
    public String getName() {
        return TerminalConst.TASK_FINISH_BY_INDEX;
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return "Finish task by index";
    }

    @Override
    public void execute() {
        System.out.println("[ENTER INDEX]");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Task task = getTaskService().finishByIndex(index);
        if (task == null) throw new TaskNotFoundException();
    }

}
