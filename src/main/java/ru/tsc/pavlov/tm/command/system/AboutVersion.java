package ru.tsc.pavlov.tm.command.system;

import ru.tsc.pavlov.tm.command.AbstractCommand;

public class AboutVersion extends AbstractCommand {

    @Override
    public String getName() {
        return "info";
    }

    @Override
    public String getArgument() {
        return "-v";
    }

    @Override
    public String getDescription() {
        return "Display program info";
    }

    @Override
    public void execute() {
        System.out.println("[VERSION]");
        System.out.println("1.17.0");
    }

}
