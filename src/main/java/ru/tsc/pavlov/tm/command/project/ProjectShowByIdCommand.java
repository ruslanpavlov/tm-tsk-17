package ru.tsc.pavlov.tm.command.project;

import ru.tsc.pavlov.tm.constant.TerminalConst;
import ru.tsc.pavlov.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.pavlov.tm.model.Project;
import ru.tsc.pavlov.tm.util.TerminalUtil;

public class ProjectShowByIdCommand extends AbstractProjectCommand {

    @Override
    public String getName() {
        return TerminalConst.PROJECT_SHOW_BY_ID;
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return "Show project by id";
    }

    @Override
    public void execute() {
        System.out.println("[ENTER ID]");
        final String id = TerminalUtil.nextLine();
        final Project project = getProjectService().findById(id);
        if (project == null) throw new ProjectNotFoundException();
        showProject(project);
    }

}
