package ru.tsc.pavlov.tm.command;

import ru.tsc.pavlov.tm.api.service.ServiceLocator;
import ru.tsc.pavlov.tm.util.IsEmptyIUtil;

public abstract class AbstractCommand {

    protected ServiceLocator serviceLocator;

    public void setServiceLocator(ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    public abstract String getName();

    public abstract String getArgument();

    public abstract String getDescription();

    public abstract void execute();

    public String toString() {

        String result = "";
        String name = getName();
        String arg = getArgument();
        String description = getDescription();

        if (!IsEmptyIUtil.IsEmpty(name)) result += name;
        if (!IsEmptyIUtil.IsEmpty(arg)) result += " " + arg + " ";
        if (!IsEmptyIUtil.IsEmpty(description)) result += " - " + description;
        return result;
    }

}
