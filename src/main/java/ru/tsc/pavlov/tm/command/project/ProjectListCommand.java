package ru.tsc.pavlov.tm.command.project;

import ru.tsc.pavlov.tm.api.service.ServiceLocator;
import ru.tsc.pavlov.tm.constant.TerminalConst;
import ru.tsc.pavlov.tm.enumerated.Sort;
import ru.tsc.pavlov.tm.model.Project;
import ru.tsc.pavlov.tm.util.IsEmptyIUtil;
import ru.tsc.pavlov.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

public class ProjectListCommand extends AbstractProjectCommand {

    @Override
    public String getName() {
        return TerminalConst.PROJECT_LIST;
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return "Display list of projects";
    }

    @Override
    public void execute() {
        System.out.println("Enter sort");
        System.out.println(Arrays.toString(Sort.values()));

        final String sort = TerminalUtil.nextLine();
        final List<Project> projects;
        if (!IsEmptyIUtil.IsEmpty(sort)) {
            Sort sortType = Sort.valueOf(sort);
            System.out.println(sortType.getDisplayName());
            projects = getProjectService().findAll(sortType.getComparator());
        }
        else {
            projects = getProjectService().findAll();
        }
        for (Project project : projects) showProject(project);
    }

}
