package ru.tsc.pavlov.tm.command.system;

import ru.tsc.pavlov.tm.command.AbstractCommand;
import ru.tsc.pavlov.tm.model.Command;

import java.util.Collection;

public class DisplayCommands extends AbstractCommand {

    @Override
    public String getName() {
        return "commands";
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return "Display list commands";
    }

    @Override
    public void execute() {
        final Collection<AbstractCommand> commands = serviceLocator.getCommandService().getCommands();
        for (final AbstractCommand command : commands)
            System.out.println(command.getName());
    }

}
