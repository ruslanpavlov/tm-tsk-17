package ru.tsc.pavlov.tm.component;

import ru.tsc.pavlov.tm.api.repository.ICommandRepository;
import ru.tsc.pavlov.tm.api.repository.IProjectRepository;
import ru.tsc.pavlov.tm.api.repository.ITaskRepository;
import ru.tsc.pavlov.tm.api.service.*;
import ru.tsc.pavlov.tm.command.AbstractCommand;
import ru.tsc.pavlov.tm.command.mutually.*;
import ru.tsc.pavlov.tm.command.project.*;
import ru.tsc.pavlov.tm.command.system.*;
import ru.tsc.pavlov.tm.command.task.*;
import ru.tsc.pavlov.tm.exception.empty.EmptyCommandException;
import ru.tsc.pavlov.tm.exception.system.UnknownCommandException;
import ru.tsc.pavlov.tm.model.Project;
import ru.tsc.pavlov.tm.model.Task;
import ru.tsc.pavlov.tm.repository.CommandRepository;
import ru.tsc.pavlov.tm.repository.ProjectRepository;
import ru.tsc.pavlov.tm.repository.TaskRepository;
import ru.tsc.pavlov.tm.service.*;
import ru.tsc.pavlov.tm.util.IsEmptyIUtil;
import ru.tsc.pavlov.tm.util.TerminalUtil;


public class Bootstrap implements ServiceLocator {

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final IProjectRepository projectRepository = new ProjectRepository();

    private final IProjectService projectService = new ProjectService(projectRepository);

    private final ITaskRepository taskRepository = new TaskRepository();

    private final ITaskService taskService = new TaskService(taskRepository);

    private final IProjectTaskService projectTaskService = new ProjectTaskService(taskRepository, projectRepository);

    private final ILogService logService = new LogService();

    {
        execute(new AboutApplication());
        execute(new AboutVersion());
        execute(new HelpCommand());
        execute(new InfoCommand());
        execute(new ExitCommand());
        execute(new DisplayArgumentsCommand());
        execute(new DisplayCommands());

        execute(new ProjectChangeStatusByIdCommand());
        execute(new ProjectRemoveByIdCommand());
        execute(new ProjectChangeStatusByIndexCommand());
        execute(new ProjectStartByNameCommand());
        execute(new ProjectChangeStatusByNameCommand());
        execute(new ProjectClearCommand());
        execute(new ProjectCreateCommand());
        execute(new ProjectFinishByIdCommand());
        execute(new ProjectFinishByIndexCommand());
        execute(new ProjectFinishByNameCommand());
        execute(new ProjectListCommand());
        execute(new ProjectShowByIdCommand());
        execute(new ProjectShowByIndexCommand());
        execute(new ProjectShowByNameCommand());
        execute(new ProjectStartByIdCommand());
        execute(new ProjectStartByIndexCommand());
        execute(new ProjectChangeStatusByNameCommand());
        execute(new ProjectUpdateByIdCommand());
        execute(new ProjectUpdateByIndexCommand());

        execute(new TaskChangeStatusByIdCommand());
        execute(new TaskChangeStatusByIndexCommand());
        execute(new TaskChangeStatusByNameCommand());
        execute(new TaskClearCommand());
        execute(new TaskCreateCommand());
        execute(new TaskFinishByIdCommand());
        execute(new TaskFinishByIndexCommand());
        execute(new TaskFinishByNameCommand());
        execute(new TaskListCommand());
        execute(new TaskRemoveByIdCommand());
        execute(new TaskRemoveByIndexCommand());
        execute(new TaskRemoveByNameCommand());
        execute(new TaskShowByIdCommand());
        execute(new TaskShowByIndexCommand());
        execute(new TaskShowByNameCommand());
        execute(new TaskStartByIdCommand());
        execute(new TaskStartByIndexCommand());
        execute(new TaskStartByNameCommand());
        execute(new TaskUpdateByIdCommand());
        execute(new TaskUpdateByIndexCommand());

        execute(new ProjectRemoveByIdCommand());
        execute(new ProjectRemoveByIndexCommand());
        execute(new ProjectRemoveByNameCommand());
        execute(new TaskAddToProjectByIdCommand());
        execute(new TaskListByProjectIdCommand());
        execute(new TaskRemoveFromProjectByIdCommand());
    }

    public void start(final String[] args) {
        logService.debug("Test environment");
        System.out.println("** WELCOME TO TASK MANAGER **");
        parseArgs(args);
        initData();
        while (true) {
            try {
                System.out.println("ENTER COMMAND");
                final String command = TerminalUtil.nextLine();
                logService.command(command);
                parseCommand(command);
                logService.info("Command complete.");
            } catch (final Exception e) {
                logService.error(e);
            }
        }

    }

    private void initData() {
        projectService.add(new Project("Project №6", "number 7"));
        projectService.add(new Project("Project №5", "number 5"));
        projectService.add(new Project("Project №3", "number 3"));
        projectService.add(new Project("Project №2", "number 2"));
        projectService.add(new Project("Project №1", "number 1"));
        projectService.add(new Project("Project №4", "number 4"));
        taskService.add(new Task("Task №6", "task number 6"));
        taskService.add(new Task("Task №1", "task number 1"));
        taskService.add(new Task("Task №5", "task number 5"));
        taskService.add(new Task("Task №3", "task number 3"));
        taskService.add(new Task("Task №4", "task number 4"));
        taskService.add(new Task("Task №2", "task number 2"));
        projectService.finishByName("Project №6");
        projectService.startByName("Project №1");
        taskService.finishByName("Task №6");
        taskService.startByName("Task №2");
    }

    public void parseCommand(final String command) {
        if (IsEmptyIUtil.IsEmpty(command)) throw new EmptyCommandException();
        AbstractCommand abstractCommand = commandService.getCommandByName(command);
        if (abstractCommand == null) throw new UnknownCommandException(command);
        abstractCommand.execute();
    }

    public void execute(final AbstractCommand command) {
        if (command == null)
            return;
        command.setServiceLocator(this);
        commandService.add(command);
    }

    public void parseArg(final String arg) {
        final AbstractCommand command = commandService.getCommandByArg(arg);
        command.execute();
    }

    public boolean parseArgs(String[] args) {
        if (IsEmptyIUtil.IsEmpty(args)) return false;
        else {
            final String arg = args[0];
            parseArg(arg);
            return true;
        }
    }

    @Override
    public ITaskService getTaskService() {
        return taskService;
    }

    @Override
    public IProjectService getProjectService() {
        return projectService;
    }

    @Override
    public IProjectTaskService getProjectTaskService() {
        return projectTaskService;
    }

    @Override
    public ICommandService getCommandService() {
        return commandService;
    }

}
